# Next.js / Material UI Template

Starter template for new projects.

Keycloak, Postgres, and WebSockets are disabled (commented-out).

## Local development

### Install dependencies
```
npm install
```

### Configure
Copy `.env` to `.env.local` and modify accordingly.

### Run server
```
npm run dev
```